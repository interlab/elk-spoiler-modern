<?php

if (!defined('ELK'))
    die('No access...');

// hook integrate_bbc_codes
function spoiler_modern(&$codes, &$no_autolink_tags, &$itemcodes)
{
    global  $modSettings, $settings, $txt;

    if (empty($modSettings['enableBBC'])) {
        return;
    }

    if (!empty($modSettings['disabledBBC'])) {
        foreach (explode(',', $modSettings['disabledBBC']) as $tag) {
            if ('spoiler' === $tag) {
                return;
            }
        }
    }

    foreach($codes as $k => $v) {
        if ('spoiler' === $v['tag']) {
            unset($codes[$k]);
        }
    }

    $ary = array(array(
            'tag' => 'spoiler',
            'before' => '<div class="spoilerheader"><img src="' . $settings['images_url'] . '/selected.png"> ' . $txt['spoiler'] . '</div><div class="spoiler"><div class="bbc_spoiler" style="display: none;">',
            'after' => '</div></div>',
            'block_level' => true,
        ),
        array(
            'tag' => 'spoiler',
            'type' => 'unparsed_equals',
            'validate' => create_function('&$tag, &$data, $disabled', '
                global $txt;
                if (\'\' === $data)
                    $data = $txt[\'spoiler\'];
            '),
            'block_level' => true,
            'before' => '<div class="spoilerheader"><img src="' . $settings['images_url'] . '/selected.png"> $1</div><div class="spoiler"><div class="bbc_spoiler" style="display: none;">',
            'after' => '</div></div>',
    ));
    $codes = array_merge($codes, $ary);
}
